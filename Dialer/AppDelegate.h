//
//  AppDelegate.h
//  Dialer
//
//  Created by Rishabh  on 18/05/16.
//  Copyright © 2016 Rishabh . All rights reserved.
//

#import     <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

