//
//  ViewController.h
//  Dialer
//
//  Created by Rishabh  on 18/05/16.
//  Copyright © 2016 Rishabh . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet UIButton *callButton;
@property (weak, nonatomic) IBOutlet UIButton *backSpace;
@property (nonatomic) NSMutableArray *contactNumber;

@end

