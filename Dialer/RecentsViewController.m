//
//  RecentsViewController.m
//  Dialer
//
//  Created by Rishabh  on 20/05/16.
//  Copyright © 2016 Rishabh . All rights reserved.
//

#import "RecentsViewController.h"
#import "ModelClass.h"

@interface RecentsViewController ()

@end

@implementation RecentsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.myTableView.delegate = self;
    self.myTableView.dataSource = self;
    self.title = @"Recents Calls";
    [self.myTableView setSeparatorColor:[UIColor redColor]];

}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.dataForTableView count];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *identifier = @"SimpleIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier];
    }
    NSString *text = [NSString stringWithFormat:@"%@",self.dataForTableView[indexPath.row]];
    cell.textLabel.text = text;
    cell.backgroundColor = [UIColor colorWithRed:18/255.0 green:84/255.0 blue:10/255.0 alpha:1.0];
    self.myTableView.backgroundColor = [UIColor colorWithRed:18/255.0 green:84/255.0 blue:10/255.0 alpha:1.0];
    [self.myTableView setSeparatorColor:[UIColor blackColor]];
    return cell;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.dataForTableView = [ModelClass getModelObject].contactArray;
    [self.myTableView reloadData];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
