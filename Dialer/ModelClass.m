//
//  ModelClass.m
//  Dialer
//
//  Created by Rishabh  on 20/05/16.
//  Copyright © 2016 Rishabh . All rights reserved.
//

#import "ModelClass.h"

@interface ModelClass()
@property ModelClass *modelClass;

@end

@implementation ModelClass

+(instancetype)getModelObject
{
    static ModelClass *sharedModelClass = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedModelClass = [[self alloc] init];
    });
    return sharedModelClass;
}

@end
