//
//  ModelClass.h
//  Dialer
//
//  Created by Rishabh  on 20/05/16.
//  Copyright © 2016 Rishabh . All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ModelClass : NSObject
@property(nonatomic)NSMutableArray *contactArray;
+(instancetype)getModelObject;

@end
