//
//  ViewController.m
//  Dialer
//
//  Created by Rishabh  on 18/05/16.
//  Copyright © 2016 Rishabh . All rights reserved.
//

#import "ViewController.h"
#import "ModelClass.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.contactNumber = [[NSMutableArray alloc]init];
    self.label.text = @" ";
    [[UITabBar appearance] setTintColor:[UIColor colorWithRed:21/255.0 green:255/255.0 blue:0 alpha:1]];
    UISwipeGestureRecognizer *gestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeHandler:)];
    self.label.userInteractionEnabled = YES;
    [gestureRecognizer setDirection:(UISwipeGestureRecognizerDirectionLeft)];
    [self.label addGestureRecognizer:gestureRecognizer];
    
//    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPress:)];
//    [self.backSpace addGestureRecognizer:longPress];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//- (void)longPress:(UILongPressGestureRecognizer*)gesture {
//    if ( gesture.state == UIGestureRecognizerStateEnded ) {
//        if(myTextOfLabel.length == 1)
//        {
//            self.label.text = @" ";
//        }else {
//            NSString *stringAfterBackSpace = [myTextOfLabel substringToIndex:myTextOfLabel.length-1];
//            self.label.text = stringAfterBackSpace;
//        }
//    }
//}

- (IBAction)button:(UIButton *)sender {
    if (self.label.text.length>10) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"You Pressed invalid number" message:@"Length is invalid" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
        [alert show];
    }
    else {
        NSString *digit = [sender currentTitle];
        //    NSLog(@"Digit Pressed = %@",digit);
        NSString *currentText = self.label.text;
        NSString *newText = [currentText stringByAppendingString:digit];
        [self.label setText:newText];
    }
}
- (IBAction)backSpace:(id)sender {
    NSString *myTextOfLabel = self.label.text;
    if(myTextOfLabel.length == 1)
    {
        self.label.text = @" ";
    }
    else {
    NSString *stringAfterBackSpace = [myTextOfLabel substringToIndex:myTextOfLabel.length-1];
    self.label.text = stringAfterBackSpace;
    }
}

- (IBAction)callButton:(id)sender {
    /*UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Can't Call" message:@"it's a Simulator" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
    [alert show];*/
    
    NSString *phoneNumber = self.label.text;
    phoneNumber = [phoneNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString *phoneStr = [NSString stringWithFormat:@"tel:%@",phoneNumber];
    NSURL *phoneURL = [NSURL URLWithString :phoneStr];
    [[UIApplication sharedApplication] openURL:phoneURL];
    
    [self.contactNumber addObject:phoneStr];
    [ModelClass getModelObject].contactArray = self.contactNumber;
}
-(void)swipeHandler:(UISwipeGestureRecognizer *)sender {
    self.label.text = @" ";
}
@end
