//
//  main.m
//  Dialer
//
//  Created by Rishabh  on 18/05/16.
//  Copyright © 2016 Rishabh . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
