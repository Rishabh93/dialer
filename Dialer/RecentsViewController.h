//
//  RecentsViewController.h
//  Dialer
//
//  Created by Rishabh  on 20/05/16.
//  Copyright © 2016 Rishabh . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecentsViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (copy,nonatomic) NSMutableArray *dataForTableView;
@property (weak, nonatomic) IBOutlet UITableView *myTableView;

@end
