//
//  DialerButton.m
//  Dialer
//
//  Created by Rishabh  on 18/05/16.
//  Copyright © 2016 Rishabh . All rights reserved.
//

#import "DialerButton.h"

@implementation DialerButton

-(void)awakeFromNib
{
    //[self applyBorders];
}

-(void) applyBorders
{
    [[self layer] setBorderWidth:1.0f];
    [[self layer] setBorderColor:[UIColor blackColor].CGColor];
    [[self layer] setCornerRadius:30.0f];
    [[self layer] setMasksToBounds:YES];

}

//- (void)drawRect:(CGRect)rect {
//    self.button = [[UIButton alloc]initWithFrame:CGRectMake(100, 100, 100, 100)];
//    self.button.backgroundColor = [UIColor whiteColor];
//    [self.button setTitle:@"1" forState:UIControlStateNormal];
//    
//}


@end
